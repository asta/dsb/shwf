.PHONY: default
default: docs run

.PHONY: run
run: build
	cabal run

.PHONY: build
build:
	cabal build

.PHONY: clean
clean:
	cabal clean
	#rm -rf ./docs

.PHONY: test
test:
	cabal test

.PHONY: docs
docs:
	#cabal haddock  --haddock-all  --htmldir="doc" --haddock-quickjump  all
	#cabal v2-haddock --haddock-html-location='https://hackage.haskell.org/package/$$pkg-$$version/docs' --haddock-hyperlink-source --haddock-quickjump
	cabal haddock  --haddock-all  --haddock-html-location='https://hackage.haskell.org/package/$$pkg-$$version/docs' --haddock-hyperlink-source --htmldir="doc" --haddock-quickjump  all
	rm -fr ./docs
	cp -r "$$(find dist-newstyle/ -type d | grep '/doc$$' | grep 'shwf' | sort -n -r | head -n 1)" ./docs
