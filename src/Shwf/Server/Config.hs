module Shwf.Server.Config ( Config(..)
                          , DBConfig(..)
                          , SessionConfig(..)
                          , AuthProviderConfig(..)
                          , readConfigFile
                          ) where

import qualified Data.ByteString.Char8 as BSC

import qualified Happstack.Lite as HL

import Data.Aeson ( Value
                  , FromJSON(..)
                  , (.:)
                  , (.:?)
                  , (.!=)
                  , withObject
                  , eitherDecodeFileStrict
                  )
import Data.Aeson.Types ( Parser )

import Database.Redis ( ConnectInfo(..)
                      , PortID(PortNumber)
                      , defaultConnectInfo
                      )
{- Example:
{
  "server": {
    "port": 8080,
    "ram_quota_bytes": 1000000,
    "disk_quota_bytes": 20000000,
    "tmp_dir": "/tmp/"
  },
  "db": {
    "type": "redis",
    "host": "localhost",
    "port": 6379,
    "auth": null,
    "select_database": 0,
    "prefix": "shwf"
  },
  "auth_providers": [
    {
      "id": "gitlab",
      "type": "gitlab_openid_connect",
      "label": "GitLab (GWDG)",
      "site": "https://gitlab.gwdg.de",
      "icon": null,
      "scope": ["openid","profile","email"],
      "app_id": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
      "app_secret": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    }
  ],
  "session": {
    "ttl_seconds": 900,
    "ttl_refreshtoken_seconds": 900,
    "jwt_secret": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    "jwt_leeway_seconds": 60
  },
  "other": null
}
-}


-------------------------
---- Types
-------------------------

data DBConfig = DBRedisConfig String ConnectInfo
  deriving (Show)

data AuthProviderConfig = AuthProviderGitLabConfig
  { authProviderID :: String
  , authProviderLabel :: String
  , authProviderSite :: String
  , authProviderIcon :: Maybe String
  , authProviderScope :: [String]
  , authProviderAppID :: String
  , authProviderAppSecret :: String
  }
  deriving (Show)

data SessionConfig = SessionConfig
  { sessionTTL :: Integer -- seconds
  , sessionTTLRefreshToken :: Integer -- seconds
  , sessionJWTSecret :: String
  , sessionJWTLeeway :: Integer -- seconds
  }
  deriving (Show)

newtype ServerConfig = ServerConfig HL.ServerConfig

data Config = Config
  { configOther :: Value
  , configServer :: ServerConfig
  , configDB :: DBConfig
  , configAuthProviders :: [AuthProviderConfig]
  , configSession :: SessionConfig
  }
  deriving (Show)


-------------------------
---- Instances
-------------------------

instance Show ServerConfig where
  show (ServerConfig sc) = "ServerConfig {"
    ++ "port = " ++ (show $ HL.port sc)
    ++ ", ramQuota = " ++ (show $ HL.ramQuota sc)
    ++ ", diskQuota = " ++ (show $ HL.diskQuota sc)
    ++ ", tmpDir = " ++ (show $ HL.tmpDir sc)
    ++ "}"

instance FromJSON Config where
  parseJSON = withObject "Config" $ \obj -> do
    other <- obj .: "other"
    server <- obj .: "server"
    db <- obj .: "db"
    authprov <- obj .: "auth_providers"
    session <- obj .: "session"
    return $ Config
      { configOther = other
      , configServer = server
      , configDB = db
      , configAuthProviders = authprov
      , configSession = session
      }

instance FromJSON SessionConfig where
  parseJSON = withObject "SessionConfig" $ \obj -> do
    ttl <- obj .: "ttl_seconds"
    ttlrefreshtoken <- obj .: "ttl_refreshtoken_seconds"
    jwtsecret <- obj .: "jwt_secret"
    jwtleeway <- obj .: "jwt_leeway_seconds"
    return $ SessionConfig { sessionTTL = ttl, sessionTTLRefreshToken = ttlrefreshtoken, sessionJWTSecret = jwtsecret, sessionJWTLeeway = jwtleeway }

instance FromJSON DBConfig where
  parseJSON = withObject "DBConfig" $ \obj -> (obj .: "type" :: Parser String) >>= \case
    "redis" -> do
      host <- obj .:? "host" .!= "localhost"
      port <- obj .:? "port" .!= 6379 -- Redis default port
      mauth <- obj .:? "auth" :: Parser (Maybe String)
      seldb <- obj .:? "select_database" .!= 0
      prefix <- obj .: "prefix"
      return $ DBRedisConfig prefix $ defaultConnectInfo
        { connectHost           = host
        , connectPort           = PortNumber $ fromInteger port
        , connectAuth           = mauth >>= Just . BSC.pack
        , connectDatabase       = seldb             -- SELECT database 0
        -- TODO , connectMaxConnections = 50              -- Up to 50 connections
        -- TODO , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
        -- TODO , connectTimeout        = Nothing         -- Don't add timeout logic
        -- TODO , connectTLSParams      = Nothing         -- Do not use TLS
        }
    t -> fail $ "Unknown/Unsupported DB Type: " ++ t

instance FromJSON ServerConfig where
  parseJSON = withObject "ServerConfig" $ \obj -> do
    port <- obj .: "port"
    ramq <- obj .:? "ram_quota_bytes" .!= 1000000
    diskq <- obj .:? "disk_quota_bytes" .!= 20000000
    tmpdir <- obj .: "tmp_dir"
    return $ ServerConfig $ HL.ServerConfig { HL.port = port
                                            , HL.ramQuota = ramq
                                            , HL.diskQuota = diskq
                                            , HL.tmpDir = tmpdir
                                            }

instance FromJSON AuthProviderConfig where
  parseJSON = withObject "AuthProviderConfig" $ \obj -> do
    provid <- obj .: "id"
    provtype <- obj .: "type"
    provlabel <- obj .: "label"
    provmicon <- obj .:? "icon"
    case provtype of
      "gitlab_openid_connect" -> do
        site <- obj .: "site"
        scope <- obj .: "scope"
        appid <- obj .: "app_id"
        appsecret <- obj .: "app_secret"
        return $ AuthProviderGitLabConfig 
          { authProviderID = provid
          , authProviderLabel = provlabel
          , authProviderIcon = provmicon
          , authProviderSite = site
          , authProviderScope = scope
          , authProviderAppID = appid
          , authProviderAppSecret = appsecret
          }
      t -> fail $ "Unknown/Unsupported Auth Provider Type: " ++ t


-------------------------
---- Helper Functions
-------------------------

readConfigFile :: FilePath -> IO Config
readConfigFile path = eitherDecodeFileStrict path >>= \case
  Left err -> fail err
  Right config -> return config











