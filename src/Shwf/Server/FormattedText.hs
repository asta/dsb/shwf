module Shwf.Server.FormattedText ( FormattedText(..)
                                 , renderFormattedText
                                 ) where

import Shwf.Server.Translation

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics

import Data.HashMap.Strict (lookup, elems)
import qualified Text.Blaze.Html5 as H
import Text.Blaze.Html5 (ToValue(..), Html, (!))
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Pretty (renderHtml)

data FormattedText = Str String
                   | LineBreak
                   | Link String [FormattedText]
                   | Strong [FormattedText]
                   | Emph [FormattedText]
                   -- TODO continue (https://hackage.haskell.org/package/pandoc-types-1.22.2.1/docs/Text-Pandoc-Definition.html#t:Inline)
  deriving (Show, Generic, ToJSON, FromJSON)
instance Translatable [FormattedText] where
  resolveTranslation _ (Untranslated ft) = renderHtml $ renderFormattedText ft
  resolveTranslation l (Translated transmap) = case Data.HashMap.Strict.lookup l transmap of
      Just res -> renderHtml $ renderFormattedText res
      Nothing -> case elems transmap of
        (x:_) -> renderHtml $ renderFormattedText x
        [] -> "(Empty Translatable)"

--data FormattedText = Str String
--                   | LineBreak
--                   | Link String [FormattedText]
--                   | Strong [FormattedText]
--                   | Emph [FormattedText]
renderFormattedText :: [FormattedText] -> Html
renderFormattedText [Str str] = H.string str
renderFormattedText [LineBreak] = H.br
renderFormattedText [Link lhref xs] = H.a ! A.href (toValue lhref) $ renderFormattedText xs
renderFormattedText [Strong xs] = H.strong $ renderFormattedText xs
renderFormattedText [Emph xs] = H.i $ renderFormattedText xs
renderFormattedText (x:xs) = renderFormattedText [x] <> renderFormattedText xs
renderFormattedText [] = mempty

