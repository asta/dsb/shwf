module Shwf.Server.Routing ( Route(..)
                           ) where

import Shwf.Server.Validation
import Shwf.Server.Validation.String
import Shwf.Server.Permission
import Shwf.Server.Session
import Shwf.Server.Templates


import qualified Happstack.Server as HS

import Data.Aeson ( ToJSON
                  , FromJSON
                  , Value
                  )


type Parser = forall a.  (Validatable a, ToJSON a, FromJSON a) => String -> Maybe a


data Guard = Method HS.Method
           | LoggedIn
           | HasPerm [Permission]


data RespStatus = Ok
                | NotFound
                | BadRequest
                | InternalServerError
                | SeeOther String

type HandlerName = String
--data Handler = SimpleHandler TemplateName (forall a. (ToJSON a) =>  HS.Request -> Session (RespStatus, a))
data Handler = SimpleHandler TemplateName (forall a. (ToJSON a) =>  HS.Request -> Session a)

--handlers :: [(HandlerName, [Guard])]
--handlers = [
--  ("user", GET, "json")
--]


data Route = Use HandlerName -- Handler to use
           | Dir String [Route] -- "users" matches /users
           | NullDir [Route] -- matches end of url (/)
           | Path StorageKey Parser [Route] -- "userid" matches /<userid>
           | StoreValue StorageKey Parser (Maybe String) [Route] -- ?key=value or key=value from Body and maybe a default value
           | StoreCookie StorageKey Parser (Maybe String) [Route]
           | StoreSomething StorageKey (forall a. a -> IO (Maybe String)) [Route]



routes :: [Route]
routes = [
    Dir "users" [ Use "Yeet" ]
    --Var "test" (Maybe (\x -> isRight $ (fromStr x :: Either VError RestrictedString)))
  ]

