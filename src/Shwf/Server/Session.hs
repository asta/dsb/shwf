module Shwf.Server.Session ( Session
                           , SessionID
                           , StorageKey
                           , StorageValue
                           ) where

import Shwf.Server.Config
import Shwf.Server.Templates
import Shwf.Server.DB

import Data.Either.Extra (eitherToMaybe)

import Control.Monad.IO.Class
import qualified Control.Monad.Trans.State as S

import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HM

import Data.Aeson ( ToJSON
                  , FromJSON
                  , Value(..)
                  , Result(..)
                  , encode
                  , decode
                  , fromJSON
                  , toJSON
                  )
import qualified Data.Vector as V

type StorageKey = String
data StorageValue = forall a. (ToJSON a, FromJSON a) => Direct a
                  | DBRef DBKey
                  | DBSetRef DBSetKey
                  -- | DBGroupRef DBGroupKey

type SessionID = String
data SessionState = SessionState {
    config :: Config
  , templates :: Templates
  , sessionID :: SessionID
  --, sessionUser :: Maybe User
  , sessionStorage :: HM.HashMap StorageKey StorageValue
  , sessionKeep :: HashSet.HashSet StorageKey
  }
--newtype Session a = Session (S.StateT SessionState IO a)
--  deriving newtype (Functor, Applicative, Monad, MonadIO)
type Session a = S.StateT SessionState IO a

getState :: Session SessionState
getState = S.get

putState :: SessionState -> Session ()
putState = S.put

getConfig :: Session Config
getConfig = (getState >>= return . config)

getTemplates :: Session Templates
getTemplates = (getState >>= return . templates)

getSessionID :: Session SessionID
getSessionID = (getState >>= return . sessionID)

--getSessionUser :: Session (Maybe User)
--getSessionUser = (getState >>= return . sessionUser)

keep :: StorageKey -> Session ()
keep key = do
  state <- getState
  putState state{sessionKeep=HashSet.insert key (sessionKeep state)}

dontkeep :: StorageKey -> Session ()
dontkeep key = do
  state <- getState
  putState state{sessionKeep=HashSet.delete key (sessionKeep state)}

j2j :: (ToJSON a, FromJSON b) => a -> Maybe b -- json to json
j2j v = resultToMaybe $ fromJSON $ toJSON v
resultToMaybe (Error _)     = Nothing
resultToMaybe (Success res) = Just res
--jl2j :: (ToJSON a, FromJSON b) => [a] -> Maybe b -- json to json
--jl2j v = j2j (Array (V.fromList $ map toJSON v))
--yeet :: (FromJSON a, ToJSON a, FromJSON b) => Either e [a] -> Maybe b
--yeet (Left _) = Nothing
--yeet (Right x) = jl2j x

resolveStorageValue :: forall a. (FromJSON a) => StorageValue -> Session (Maybe a)
resolveStorageValue (Direct v) = return $ j2j v
resolveStorageValue (DBRef key) = do
  state <- getState
  res <- liftIO $ dbFetch (configDB $ config state) key
  return $ eitherToMaybe res
resolveStorageValue (DBSetRef key) = do
  state <- getState
  res <- liftIO $ dbSetMembers (configDB $ config state) key
  return $ j2j $ eitherToMaybe res
--resolveStorageValue (DBGroupRef key) = do
--  state <- getState
--  res <- liftIO $ dbFetchAll (configDB $ config state) key
--  return $ yeet res

get :: FromJSON a => StorageKey -> Session (Maybe a)
get key = do
  state <- getState
  return $ Nothing

