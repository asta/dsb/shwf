module Shwf.Server.Validation.Number ( PositiveInt , pattern PPositiveInt
                                     --, Double(..)
                                     ) where

import Shwf.Server.Validation

import Text.Read

import Data.Aeson as JSON
import GHC.Generics

-- Positive Number with minimum value 0
newtype PositiveInt = PositiveInt Int
  deriving          (Generic, Eq)
  deriving anyclass (ToJSON, FromJSON)
  deriving newtype  (Show)
instance Validatable PositiveInt where
  fromStr str = case readEither str :: Either String Int of
    Left err -> Left $ VErrorReadFailed err
    Right int -> validate $ PositiveInt int
  validate resint@(PositiveInt int) | int < 0  = Left $ VErrorTooSmall 0 int
                                    | otherwise = Right resint
pattern PPositiveInt :: Int -> PositiveInt
pattern PPositiveInt int = PositiveInt int
{-# COMPLETE PPositiveInt #-}

--instance Validatable Double where
--  fromStr str = case readEither str :: Either String Double of
--    Left err -> Left $ VErrorReadFailed err
--    Right val -> validate $ val
--  validate val | isNaN val = Left VErrorInvalidRealFloat
--               | isInfinite val = Left VErrorInvalidRealFloat
--               | isDenormalized val = Left VErrorInvalidRealFloat
--               | not $ isIEEE val = Left VErrorInvalidRealFloat
--               | otherwise = Right val
--
