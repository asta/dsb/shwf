module Main (main) where

import Shwf.Server.Config

main :: IO ()
main = do
  config <- readConfigFile "test/config.json"
  print config

